﻿using System.Web.Mvc;

namespace SpiritualSupport.Helpers
{
    public class ExceptionLoggerAttribute : FilterAttribute, IExceptionFilter
    {
        public static string ErrorMessages;
        public void OnException(ExceptionContext filterContext)
        {
            var exceptionMessage = filterContext.Exception.Message;

            ErrorMessages = $"{exceptionMessage}";
            filterContext.ExceptionHandled = true;
            filterContext.Result = new RedirectResult("/Home/ErrorInform");
        }
    }
}
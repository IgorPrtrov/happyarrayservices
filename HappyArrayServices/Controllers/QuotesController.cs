﻿using System;
using System.Web.Mvc;

namespace SpiritualSupport.Controllers
{
    public class QuotesController : Controller
    {
        [AllowAnonymous]
        public ActionResult Get()
        {
            var quots = Resources._1000_quots.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            Random rnd = new Random();
            int unicq = rnd.Next(0, quots.Length);

            var quot = quots[unicq];
            ViewBag.Quot = quot;

            return View();
        }
    }
}